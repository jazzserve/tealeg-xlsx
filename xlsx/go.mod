module bitbucket.org/jazzserve/tealeg-xlsx/xlsx/v3

go 1.13

require (
	github.com/frankban/quicktest v1.10.0
	github.com/google/btree v1.0.0 // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/peterbourgon/diskv v2.0.1+incompatible
	github.com/rogpeppe/fastuuid v1.2.0
	github.com/shabbyrobe/xmlwriter v0.0.0-20200208144257-9fca06d00ffa
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f
)
